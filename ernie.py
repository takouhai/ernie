from pathlib import Path
import itertools
import threading
import time
import sys
from transformers import *

file = input("What file do you want ERNIE to read? ")
percent = input("How much should ERNIE remember from your article? (%) ")
dir = 'texts/' + file
txt = Path(dir).read_text()
txt_ratio = int(percent)/100

bert = "\n      \\WWW/\n      /   \\ \n     /wwwww\\ \n   _|  o_o  |_ \n  (_   / \\   _)  \"Hey, ERNIE, you should read this interesting article \n    |  \\_/  |    titled '{}'! It's kind of long...\"  \n    : ----- : \n    : \\___/ : \n     \\_____/ \n     [     ]\n     `\"\"\"\"\"`\n".format(file)

print(bert)
done = False
#here is the animation
def animate():
    for c in itertools.cycle(['|', '/', '-', '\\']):
        if done:
            break
        sys.stdout.write('\rReading and summarizing... ' + c)
        sys.stdout.flush()
        time.sleep(0.1)
    sys.stdout.write('\rDone!     ')

t = threading.Thread(target=animate)
t.start()
time.sleep(10)

# Load model, model config and tokenizer via Transformers
custom_config = AutoConfig.from_pretrained('bert-large-uncased')
custom_config.output_hidden_states=True
custom_tokenizer = AutoTokenizer.from_pretrained('bert-large-uncased')
custom_model = AutoModel.from_pretrained('bert-large-uncased', config=custom_config)

from summarizer import Summarizer


model = Summarizer(custom_model=custom_model, custom_tokenizer=custom_tokenizer)
result = model(txt, ratio=txt_ratio)
full = ''.join(result)
done = True

ernie="\n\n    \\WWWWWWW/\n  _/`  o_o  `\\_\n (_    (_)    _)\n   : _______ :   \"I read that really long article \n   \\ '-...-' /   you sent me titled '{}', BERT! I think \n   (`'-----'`)   it went kind of like this...\"\n    `\"\"\"\"\"\"\"` \n".format(file)
print(ernie)
print("\n==========================================================\n")
print(full)
print("\n==========================================================\n")